import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormureactComponent } from './formureact.component';

describe('FormureactComponent', () => {
  let component: FormureactComponent;
  let fixture: ComponentFixture<FormureactComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormureactComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormureactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
