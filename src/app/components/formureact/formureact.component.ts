import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup,FormArray } from '@angular/forms';

@Component({
  selector: 'app-formureact',
  templateUrl: './formureact.component.html',
  styleUrls: ['./formureact.component.css']
})
export class FormureactComponent implements OnInit {

  formulario!:FormGroup;
  constructor(private fb:FormBuilder) { 
    this.crearFormulario();
  }

  get nombres1(){
    return this.formulario.get('names') as FormArray;
  }
  ngOnInit(): void {
     
  }

  crearFormulario(){
    this.formulario= this.fb.group({
      name:[''],
      names:this.fb.array([['Seleccione..'],[]]),
      mostrar:['']
    })
  }


}
